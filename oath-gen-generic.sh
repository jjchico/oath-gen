#!/bin/sh

user_key=2FAKey

otp=$(oathtool \
    --totp=SHA1 --digits=6 --time-step-size=30 --base32 \
    $user_key)

# Copy OTP to X selection and clipboard
echo "$otp" | xclip
xclip -o | xclip -selection clipboard

# Show the OTP to the user
/usr/bin/zenity \
  --title "2FA Key" \
  --icon oath-gen \
  --timeout=10 \
  --info \
  --text "<span font=\"40\"><b>$otp</b></span>\n(Copyed to clipboard)"
