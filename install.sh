#!/bin/sh

if ! (which oathtool > /dev/null 2>&1); then
    echo "Please install 'oathtool'."
    exit 1
fi
if ! (which zenity > /dev/null 2>&1); then
    echo "Please install 'zenity'."
    exit 1
fi

echo "Paste the user's secret in this terminal and press ENTER."
echo "Or press Ctrl-C to abort the install."

read key
sed "s/2FAKey/$key/" oath-gen-generic.sh > oath-gen.sh

chmod 700 oath-gen.sh
mkdir -p $HOME/bin/
mv oath-gen.sh $HOME/bin/
mkdir -p $HOME/.local/share/icons/
cp oath-gen.svg $HOME/.local/share/icons/
desktop-file-install --dir $HOME/.local/share/applications/ oath-gen.desktop
chmod 700 $HOME/.local/share/applications/oath-gen.desktop

echo "'oath-gen' should be installed now. Test it in your launcher."
