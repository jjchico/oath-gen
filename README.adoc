= Interface for double factor (2FA) authentication key generation

This is a configuration for easy generation of 2FA single use keys in
Ubuntu/Linux systems (and maybe others) using *oathtool*.

== Installation

=== Automatic install

. Install packages *oathtool* and *zenity* if not already installed:

    $ sudo apt install oathtool zenity

. Download the files of this project in a folder.

. Obtain the user's 2FA key from the service provider.

Running the `install.sh` script from the project's folder will complete the
install for you but you may try the manual install instructions below.

    $ ./install.sh

=== Manual install

. Edit the file `oath-gen-generic.sh` and substitute the *2FAKey* string by the
user's 2FA key that you want to configure. You may have to change other options
of *oathtool* here. Check with your service provider. The current options work
with the 2FA services of the https://www.us.es[University of Seville] at the
time of writing this.

. Rename (or copy) the `oath-gen-generic.sh` file to `oath-gen.sh`.

. Make the file executable:

    $ chmod u+x oath-gen.sh

. Move (or copy) the files to the following routes, creating the folders if
necessary:
  * `oath-gen.sh` -> `$HOME/bin/`
  * `oath-gen.svg` -> `$HOME/.local/share/icons/`
  * `oath-gen.desktop` -> `$HOME/.local/share/applications/`

    $ mkdir -p $HOME/bin/
    $ cp oath-gen.sh $HOME/bin/
    $ mkdir -p $HOME/.local/share/icons/
    $ cp oath-gen.svg $HOME/.local/share/icons/
    $ desktop-file-install --dir $HOME/.local/share/applications/ oath-gen.desktop

. Update the applications database:

    $ update-desktop-database $HOME/.local/share/applications/

=== Test

Execute the one-time key generator by searching things like *oath* or *2fa* in
your launcher.

=== Uninstall

Execute the `uninstall.sh` script from the projects folder:

    $ ./uninstall.sh

== Credits

  * https://commons.wikimedia.org/w/index.php?curid=674766[Logo] from project
  Tango, in the public domain.

== Author

  * Jorge Juan-Chico <jjchico@dte.us.es>

== License

The contents of this project is in the public domain. The author does not claim
it works correctly. Use it under your own responsibility.
