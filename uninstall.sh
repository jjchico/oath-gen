#!/bin/sh

rm -f $HOME/bin/oath-gen.sh
rm -f $HOME/.local/share/icons/oath-gen.svg
rm -f $HOME/.local/share/applications/oath-gen.desktop

echo "'oath-gen' should be uninstalled now. Test it in your launcher."